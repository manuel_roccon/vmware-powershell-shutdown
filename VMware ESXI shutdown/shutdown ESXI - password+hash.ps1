#################################################################################
# Shutdown VMs and Hosts
#
# This script will loop through a list of ESXi hosts and initiate shutdown
# commands to the vm's residing on them. If VMware Tools is installed, the
# script will attempt to do a graceful shutdown. If VMware tools is not
# installed a hard power off will be issued. One note, if you have any VMs that
# you would like to remain on until the end of the process be sure to put their
# names in the $vmstoleaveon variable and also be sure they reside on the last
# host listed in the $hoststoprocess variable. If you run this script in a VM of
# a host, you must include this in $vmstoleaveon.
# if any VMs are still on afterdoing initial shutdown, then runs a hard stop, 
# and enters maintenance mode before shutting down.
#
#
#################################################################################

if ( (Get-PSSnapin -Name VMware.VimAutomation.Core -ErrorAction SilentlyContinue) -eq $null )
{
    #Add-PsSnapin VMware.VimAutomation.Core
	Import-Module  VMware.VimAutomation.Core
}else{
	Write-Host "PsSnapin not found, now I try to download it automaticaly (need administrator right)...."
	Install-Module -Name VMware.VimAutomation.Core
	if ( (Get-PSSnapin -Name VMware.VimAutomation.Core -ErrorAction SilentlyContinue) -eq $null )
	{
		#Add-PsSnapin VMware.VimAutomation.Core
		Import-Module  VMware.VimAutomation.Core
	}else{
		Write-Host "Unable to download PS Snapin, download it from https://code.vmware.com/web/tool/12.0.0/vmware-powercli"
		exit
	}
}

#select if you want to use clear credentials or hash to connect to the host (use true or false)
$useclearcredentials = "true";

# path where hash file is stored
$ScriptFolder = "<C:\hash>"

# ESXi Username
$username = "<IP or FQDN>"

# ESXi Password
$password = "<password>"

# list of hosts to process (use IP or HOST NAME)
$listofhosts = "<esxi-01>", "<esxi-02>", "<esxi-03>", "<192.168.0.240>"

# list of vm's to go down with the ship' - vms must reside on last host in above list!.
# if you run this script in a vm inside a host insert this vm here
$vmstoleave = "<VM1>", "<VM2>"

# seconds before script force shutdown vms before shutdown ESXi hosts
$timeout = 100

#set if invalid server certificate error
Set-PowerCLIConfiguration -InvalidCertificateAction Ignore -Confirm:$false

#loop through each host
Foreach ($esxhost in $listofhosts)
{
    #Connect to the VMWare Server
    if($useclearcredentials -eq "true"){
        #using clear credentials to connect to hosts
        Connect-VIServer -Server $esxhost -user $username -pass $password
    }else{
        #using hashed credentials to connect to hosts
        $ESXPassword=Get-Content $ScriptFolder\cred.txt | ConvertTo-SecureString
        $ESXCredentials=New-Object -Typename System.Management.Automation.PSCredential -Argumentlist $username, $ESXPassword
        Connect-VIServer -Server $esxhost  -Credential $ESXCredentials
    }

    $currentesxhost = get-vmhost $esxhost
    Write-Host "Processing $currentesxhost"

    #loop through each vm on host
Foreach ($VM in ($currentesxhost | Get-VM | where { $_.PowerState -eq "PoweredOn" }))
    {
        Write-Host "===================================================================="
        Write-Host "Processing $vm"
 
        # if this is a vm that is supposed to go down with the ship.
        if ($vmstoleave -contains $vm)
        {
            Write-Host "I am $vm - I will go down with the ship"
        }
        else
        {
            Write-Host "Checking VMware Tools."
            $vminfo = get-view -Id $vm.ID
            
            # If we have VMware tools installed
            if ($vminfo.config.Tools.ToolsVersion -eq 0)
            {
                Write-Host "$vm doesn't have vmware tools installed, hard power this one"
                
                # Hard Power Off
                Stop-VM $vm -confirm:$false
            }
            else
            {
                write-host "I will attempt to shutdown $vm"

                # Power off gracefully
                $vmshutdown = $vm | shutdown-VMGuest -Confirm:$false
            }
        }
        Write-Host "===================================================================="
    }
    Write-Host "Initiating host shutdown in $timeout seconds"
    sleep $timeout
    
    #look for other vm's still powered on.
    Foreach ($VM in ($currentesxhost | Get-VM | where { $_.PowerState -eq "PoweredOn" }))
    {

        # if this is a vm that is supposed to go down with host.
        if ($vmstoleave -contains $VM)
        {
            Write-Host "I am $vm - I will go down with the host"
        }else{
            Write-Host "===================================================================="
            Write-Host "Processing $vm"
            Stop-VM $vm -confirm:$false
            Write-Host "===================================================================="
        }
    }
    
    #Shut down the host
    Sleep 20
    Set-VMhost -VMhost $currentesxHost -State Maintenance
    Sleep 15
    $currentesxhost | Foreach {Get-View $_.ID} | Foreach {$_.ShutdownHost_Task($TRUE)}

}
Write-Host "Shutdown Complete"
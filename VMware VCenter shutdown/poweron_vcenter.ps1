#################################################################################
# Power On the Powered Off VMs (poweronvms.ps1)
#
# Variables:  $useClearCredentials - Specify if script use clear or hashed credentials (see informations about System.Management.Automation.PSCredential)
#			  $scriptFolder - specify folder where is credentials hash file
#             $vcenter - The IP/DNS of your vCenter Server
#             $username/password - credentials for your vCenter Server
#             $filename - name of the file that poweroffvms.ps1 dumps
#             $myImportantVMs - list of VMS to be powered on first
#
#			  This script is used if you not want to use autostart funtion of Vcenter
#       
#
#################################################################################

if ( (Get-PSSnapin -Name VMware.VimAutomation.Core -ErrorAction SilentlyContinue) -eq $null )
{
    #Add-PsSnapin VMware.VimAutomation.Core
	Import-Module  VMware.VimAutomation.Core
}else{
	Write-Host "PsSnapin not found, now I try to download it automaticaly (need administrator right)...."
	Install-Module -Name VMware.VimAutomation.Core
	if ( (Get-PSSnapin -Name VMware.VimAutomation.Core -ErrorAction SilentlyContinue) -eq $null )
	{
		#Add-PsSnapin VMware.VimAutomation.Core
		Import-Module  VMware.VimAutomation.Core
	}else{
		Write-Host "Unable to download PS Snapin, download it from https://code.vmware.com/web/tool/12.0.0/vmware-powercli"
		exit
	}
}

# VARIABILES

#select if you want to use clear credentials or hash to connect to the host (use true or false)
$useClearCredentials = "true";

# path where hash file is stored
$scriptFolder = "C:\hash"

# specify VCenter host
$vcenter = "<vcenter.mgm.local>"

# specify VCenter username
$username = "<administrator@mgm.local>"

# specify VCenter username (not needed if use hash login)
$password = "<password>"

#specify where script save poweron machine information
$filename = "c:\path\to\poweredonguests.csv"

# specify important VM to power on
$myImportantVMs = "<SQLSERVER01>", "<DHCPSERVER02>"

#set if invalid server certificate error
Set-PowerCLIConfiguration -InvalidCertificateAction Ignore -Confirm:$false

Write-Host "Power on Phisical Hosts" -nonewline
#INSERT HERE EXTERNAL COMMAND TO POWER ON HOSTS and SAN
#######################################################
Sleep 2

#check if the power on file exists - if it does, then there was a power outage... 
Write-Host "Checking to see if Power Off dump file exists....." -nonewline
Sleep 2

if (Test-Path $filename)
{
    Write-Host "File Found" -Foregroundcolor green
    Write-Host ""
    $date = ( get-date ).ToString('yyyyMMdd-HHmmss')
    
	#check if vCenter service is running
    Write-Host "Checking for vCenter Service..." -nonewline
    Sleep 2
    
	while ((Get-Service vpxd).Status -ne "Running")
    {
        Write-Host "." -nonewline
        Sleep 2
        Write-Host "." -nonewline
        Sleep 2
        Write-Host "." -nonewline
        Sleep 2 
    }
    
	Write-Host "Service has Started!" -ForegroundColor Green 
    
	#connect to vcenter
    Sleep 5
    
	Write-Host "Connecting to vCenter Server..." -nonewline
    Sleep 3
	
	#Connect to the VMWare Server
    if($useClearCredentials -eq "true"){
        
		#using clear credentials to connect to hosts
        $success = Connect-VIServer $vcenter -username $username -Password $password -WarningAction:SilentlyContinue
    }else{
        
		#using hashed credentials to connect to hosts
        $VCPassword=Get-Content $scriptFolder\cred.txt | ConvertTo-SecureString
        $VCCredentials=New-Object -Typename System.Management.Automation.PSCredential -Argumentlist $username, $VCPassword
        $success = Connect-VIServer $vcenter  -Credential $VCCredentials -WarningAction:SilentlyContinue
    }
	
    if ($success) 
    { 
        Write-Host "Connected" -ForegroundColor Green 
    }
    else 
    { 
        Write-Host "ISSUES, aborting script" -Foregroundcolor Red 
        exit
    }
    
	Write-Host ""
    Write-Host "Starting the most important VMs first (Phase 1)" -ForegroundColor Green
    Write-Host ""
    
	foreach ($iVM in $myImportantVMs)
    {
        Write-Host "Powering on $iVM ..." -nonewline
        Start-VM $iVM
        Sleep 5
        Write-Host "DONE" -Foregroundcolor Green   
    }
    
	Write-Host ""
    Write-Host "Starting the remaining VMs" -ForegroundColor Green
    Write-Host ""
    
	#read file and start VMs every 5 seconds...
    $vms = Import-CSV $filename
    foreach ($vm in $vms)
    {
        
		$vmname = $vm.Name
        
		if ($myImportantVMs -notcontains $vmName)
        {
            
			Start-VM $vm.Name
            Write-Host "Powering on $vmName " 
            sleep 5
            Write-Host "DONE" 
        
		}
        else
        {
			Write-Host "Skipping $vmname - already powered on in phase 1" -Foregroundcolor yellow
        }
    }
    
	Write-Host "Power on completed, I will now rename the dump file...." -nonewline
    $DateStamp = get-date -uformat "%Y-%m-%d@%H-%M-%S"  
    $fileObj = get-item $fileName
    $extOnly = $fileObj.extension
    $nameOnly = $fileObj.Name.Replace( $fileObj.Extension,'')
    rename-item "$fileName" "$nameOnly-$DateStamp$extOnly"
    Write-Host "DONE" -foregroundcolor green
    Write-Host "File has been renamed to $nameOnly-$DateStamp$extOnly"   

}
else
{
    Write-Host "File Not Found - aborting..." -Foregroundcolor green
    exit
}
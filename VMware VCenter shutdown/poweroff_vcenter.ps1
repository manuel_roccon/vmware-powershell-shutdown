#################################################################################
# Power off VMs
#
# Variables:  $useClearCredentials - Specify if script use clear or hashed credentials (see informations about System.Management.Automation.PSCredential)
#			  $scriptFolder - specify folder where is credentials hash file
#			  $vcenter - The IP/DNS of your vCenter Server
#             $username/password - credentials for your vCenter Server
#             $filename - path to csv file to store powered on vms
#             used for the poweronvms.ps1 script.
#             $cluster - Name of specific cluster to target within vCenter
#             $datacenter - Name of specific datacenter to target within vCenter
#
#################################################################################
  
if ( (Get-PSSnapin -Name VMware.VimAutomation.Core -ErrorAction SilentlyContinue) -eq $null )
{
    #Add-PsSnapin VMware.VimAutomation.Core
	Import-Module  VMware.VimAutomation.Core
}else{
	Write-Host "PsSnapin not found, now I try to download it automaticaly (need administrator right)...."
	Install-Module -Name VMware.VimAutomation.Core
	if ( (Get-PSSnapin -Name VMware.VimAutomation.Core -ErrorAction SilentlyContinue) -eq $null )
	{
		#Add-PsSnapin VMware.VimAutomation.Core
		Import-Module  VMware.VimAutomation.Core
	}else{
		Write-Host "Unable to download PS Snapin, download it from https://code.vmware.com/web/tool/12.0.0/vmware-powercli"
		exit
	}
}

# VARIABILES
 
# select if you want to use clear credentials or hash to connect to the host (use true or false)
$useClearCredentials = "true";

# path where hash file is stored
$scriptFolder = "C:\hash"

# specify VCenter host
$vcenter = "<vcenter.mgm.local>"

# specify VCenter username
$username = "<administrator@mgm.local>"

# specify VCenter username (not needed if use hash login)
$password = "<password>"

# specify VCenter Cluster
$cluster = "<Cluster>"

# specify VCenter Datacenter
$datacenter = "<Datacenter>"

#specify where script save shoutdown machine information
$filename = "c:\shutdown\poweredonvms.csv"

# For use with a virtualized vCenter - bring it down last- if you don't need this functionality
# simply leave the variable set to NA and the script will work as it always had
$vCenterVMName = "<VCENTER>"

# specify if you want to shutdown VCENTER ESXI in last step (if you have several cluster in VM, ESXI host with Vcenter must be shutdown last)
$vCenterShutdown = "YES"

# specify if you whant shut down only host inside a cluster or all vm in datacenter (cluster hosts and standalone hosts) YES/NO
# you can use this feature only if you have only one cluster in Vcenter!
$shutdownOnlyClusterHosts = "YES"

# customize log file
$time = ( get-date ).ToString('HH-mm-ss')
$date = ( get-date ).ToString('dd-MM-yyyy')
$logfile = New-Item -type file "C:\shutdown\ShutdownLog-$date-$time.txt" -Force

#set if invalid server certificate error
Set-PowerCLIConfiguration -InvalidCertificateAction Ignore -Confirm:$false

Write-Host ""
Write-Host "Shutdown command has been sent to the vCenter Server." -Foregroundcolor yellow
Write-Host "This script will shutdown all of the VMs and hosts located in $datacenter." -Foregroundcolor yellow
Write-Host "Upon completion, this server ($vcenter) will also be shutdown gracefully." -Foregroundcolor yellow
Write-Host "===================================================================="
Sleep 5
  
Add-Content $logfile "$(get-date -f dd/MM/yyyy) $(get-date -f HH:mm:ss) PowerOff Script Engaged"
Add-Content $logfile ""
  
# Connect to vCenter
Write-Host "Connecting to vCenter - $vcenter.... " -nonewline
Add-Content $logfile "$(get-date -f dd/MM/yyyy) $(get-date -f HH:mm:ss) Connecting to vCenter - $vcenter"
 
if($useClearCredentials -eq "true"){
    #using clear credentials to connect to hosts
    $success = Connect-VIServer $vcenter -username $username -Password $password -WarningAction:SilentlyContinue
}else{
    #using hashed credentials to connect to hosts
    $VCPassword=Get-Content $scriptFolder\cred.txt | ConvertTo-SecureString
    $VCCredentials=New-Object -Typename System.Management.Automation.PSCredential -Argumentlist $username, $VCPassword
    $success = Connect-VIServer $vcenter  -Credential $VCCredentials -WarningAction:SilentlyContinue
}
	
if ($success) { 
    Write-Host "Connected!" -Foregroundcolor Green 
}
else
{
    Write-Host "Something is wrong, Aborting script" -Foregroundcolor Red
    Add-Content $logfile "$(get-date -f dd/MM/yyyy) $(get-date -f HH:mm:ss) Something is wrong, Aborting script"
    exit
}

# connected to vcenter
Write-Host "===================================================================="
Write-Host "Beginning Phase 1 - Stop vApps, change DRS, disable HA...." -Foregroundcolor green
Write-Host ""
Add-Content $logfile  ""
  
# Turn Off vApps
Write-Host "Stopping VApps...." -Foregroundcolor Green
Add-Content $logfile "$(get-date -f dd/MM/yyyy) $(get-date -f HH:mm:ss) Stopping VApps..."
Add-Content $logfile ""
  
$vapps = Get-VApp | Where { $_.Status -eq "Started" }
  
if ($vapps -ne $null)
{
    ForEach ($vapp in $vapps)
    {
            Write-Host "Processing $vapp.... " -ForegroundColor Green
            Write-Host ""
            Add-Content $logfile "$(get-date -f dd/MM/yyyy) $(get-date -f HH:mm:ss) Stopping $vapp."
            Add-Content $logfile ""
            Stop-VApp -VApp $vapp -Confirm:$false | out-null
            Write-Host "$vapp stopped." -Foregroundcolor Green
            Write-Host ""
    }
}
  
Write-Host "VApps stopped." -Foregroundcolor Green
Add-Content $logfile "$(get-date -f dd/MM/yyyy) $(get-date -f HH:mm:ss) VApps stopped."
Add-Content $logfile ""
Write-Host ""
 
# Get a list of all powered on VMs - used for powering back on....
Get-VM -Location $cluster | where-object {$_.PowerState -eq "PoweredOn" } | Select Name | Export-CSV $filename
  
# Change DRS Automation level to partially automated...
Write-Host "Changing cluster DRS Automation Level to Partially Automated" -Foregroundcolor green
Get-Cluster $cluster | Set-Cluster -DrsAutomation PartiallyAutomated -confirm:$false
  
# Change the HA Level
Write-Host ""
Write-Host "Disabling HA on the cluster..." -Foregroundcolor green
Write-Host ""
Add-Content $logfile "Disabling HA on the cluster..."
Add-Content $logfile ""
Get-Cluster $cluster | Set-Cluster -HAEnabled:$false -confirm:$false
  
# Get VMs again (we will do this again instead of parsing the file in case a VM was powered in the nanosecond that it took to get here....
Write-Host ""
Write-Host "Retrieving a list of powered on guests...." -Foregroundcolor Green
Write-Host ""
Add-Content $logfile "$(get-date -f dd/MM/yyyy) $(get-date -f HH:mm:ss) Retrieving a list of powered on guests...."
Add-Content $logfile ""
$poweredonguests = Get-VM -Location $cluster | where-object {$_.PowerState -eq "PoweredOn" }
  
Add-Content $logfile "$(get-date -f dd/MM/yyyy) $(get-date -f HH:mm:ss) Checking to see if vCenter is virtualized"
 
# Retrieve Esxi host info for vCenter VM
Write-Host "Retrieve Esxi host info for vCenter VM"
if ($vcenterVMName -ne "NA")
{
    Add-Content $logfile "$(get-date -f dd/MM/yyyy) $(get-date -f HH:mm:ss) vCenter is indeed virtualized, getting ESXi host hosting vCenter Server"
    #$vCenterHost = (Get-VM $vCenterVMName).Host.Name
	$vCenterHost = (Get-VM $vCenterVMName).VMHost.Name
    Add-Content $logfile "$(get-date -f dd/MM/yyyy) $(get-date -f HH:mm:ss) $vCenterVMName currently running on $vCenterHost - will process this last"
	Write-Host "$vCenterVMName currently running on $vCenterHost - will process this last"
    Write-Host ""
}
else
{
    Add-Content $logfile "$(get-date -f dd/MM/yyyy) $(get-date -f HH:mm:ss) vCenter is not virtualized"
	Write-Host "vCenter is not virtualized"
}

Add-Content $logfile "$(get-date -f dd/MM/yyyy) $(get-date -f HH:mm:ss) Proceeding with VM PowerOff"
Add-Content $logfile ""
  
# start powering off Cluser VMs....
Write-Host "===================================================================="
Write-Host "Beginning Phase 2 - Start powering off cluster VMs...." -Foregroundcolor green
Write-Host ""
ForEach ( $guest in $poweredonguests )
{
    if ($guest.Name -ne $vCenterVMName)
    {
        Write-Host "Processing $guest.... " -ForegroundColor Green
        Write-Host "Checking for VMware tools install" -Foregroundcolor Green
        $guestinfo = get-view -Id $guest.ID
        if ($guestinfo.config.Tools.ToolsVersion -eq 0)
        {
            Write-Host "No VMware tools detected in $guest , hard power this one" -ForegroundColor Yellow
            Write-Host ""
            Add-Content $logfile "$(get-date -f dd/MM/yyyy) $(get-date -f HH:mm:ss) $guest - no VMware tools, hard power off"
            Stop-VM $guest -confirm:$false | out-null
        }
        else
        {
           write-host "VMware tools detected.  I will attempt to gracefully shutdown $guest"
           Write-Host ""
           Add-Content $logfile "$(get-date -f dd/MM/yyyy) $(get-date -f HH:mm:ss) $guest - VMware tools installed, gracefull shutdown"
           $vmshutdown = $guest | shutdown-VMGuest -Confirm:$false | out-null
        }
    }
}
  
# Let's wait a minute or so for shutdowns to complete
Write-Host "Giving VMs 2 minutes before resulting in hard poweroff"
Write-Host ""
Add-Content $logfile ""
Add-Content $logfile "$(get-date -f dd/MM/yyyy) $(get-date -f HH:mm:ss) Waiting a couple minutes then hard powering off all remaining VMs"
Sleep 120
  
Write-Host "===================================================================="
# Now, let's go back through again to see if anything is still powered on and shut it down if it is
Write-Host "Beginning Phase 3 - shout down all VM running yet on cluster $cluster...." -ForegroundColor red
Write-Host ""
  
# Get our list of guests still powered on and force stop VMs...
$poweredonguests = Get-VM -Location $cluster | where-object {$_.PowerState -eq "PoweredOn" }
  
if ($poweredonguests -ne $null)
{
    ForEach ( $guest in $poweredonguests )
    {
        if ($guest.Name -ne $vCenterVMName)
        {
            Write-Host "Processing $guest ...." -ForegroundColor Green
            #no checking for toosl, we just need to blast it down...
            write-host "Shutting down $guest - I don't care, it just needs to be off..." -ForegroundColor Yellow
            Write-Host ""
            Add-Content $logfile "$(get-date -f dd/MM/yyyy) $(get-date -f HH:mm:ss) $guest - Hard Power Off"
            Stop-VM $guest -confirm:$false | out-null
        }
    }
}
  
# Wait 30 seconds
Write-Host "Waiting 30 seconds and then proceding with host power off"
Write-Host ""
Add-Content $logfile ""
Add-Content $logfile "$(get-date -f dd/MM/yyyy) $(get-date -f HH:mm:ss) Processing power off of all hosts now"
Sleep 30
  
# and now its time to shut down the hosts - I've chosen to go by datacenter here but you could put the cluster
# There are some standalone hosts in the datacenter that I would also like to shutdown, those vms are set to
# start and stop with the host, so i can just shut those hosts down and they will take care of the vm shutdown
 
Write-Host "===================================================================="
Write-Host "Beginning Phase 4 - Shutdown all VMs in Datacenter without virtualized vCenter - only cluster hosts affected" -ForegroundColor Green
Write-Host ""
foreach ($esxhost in $esxhosts)
{
    if ($esxhost -ne $vCenterHost)
    {
        #Shutem all down
        Write-Host "Shutting down $esxhost" -ForegroundColor Green
        #Add-Content $logfile "$(get-date -f dd/MM/yyyy) $(get-date -f HH:mm:ss) Shutting down $esxhost"
        $esxhost | Foreach {Get-View $_.ID} | Foreach {$_.ShutdownHost_Task($TRUE)}
    }
}

# optional - you can use specific sript for stand alone ESXI
if ($shutdownOnlyClusterHosts -eq "NO")
{
	Write-Host "Beginning Phase 4a - Shutdown all VMs in Datacenter without virtualized vCenter - affected cluster and standalone hosts" -ForegroundColor Green
	$esxhosts = Get-VMHost -Location $datacenter
    Write-Host ""
    foreach ($esxhost in $esxhosts)
    {
        if ($esxhost -ne $vCenterHost)
        {
            #Shutem all down
            Write-Host "Shutting down $esxhost" -ForegroundColor Green
            #Add-Content $logfile "$(get-date -f dd/MM/yyyy) $(get-date -f HH:mm:ss) Shutting down $esxhost"
            $esxhost | Foreach {Get-View $_.ID} | Foreach {$_.ShutdownHost_Task($TRUE)}
        }
    }
    
}


# Shut down the host which vCenter resides on if using that functionality
Write-Host "===================================================================="
Write-Host "Beginning Phase 5 - Shut down the host which vCenter resides on if using that functionality" -ForegroundColor Green
Write-Host ""
if ($vCenterShutdown -eq "YES")
{
	if ($vcenterVMName -ne "NA")
	{
		Add-Content $logfile ""
		Add-Content $logfile "$(get-date -f dd/MM/yyyy) $(get-date -f HH:mm:ss) Processing $vcenterHost - shutting down "
		Write-Host "Processing $vcenterHost - shutting down "
		Get-VMhost $vCenterHost | Foreach {Get-View $_.ID} | Foreach {$_.ShutdownHost_Task($TRUE)} | out-null
		Write-Host "Shutdown Complete." -ForegroundColor Green
	}
}
  
Add-Content $logfile ""
Add-Content $logfile "$(get-date -f dd/MM/yyyy) $(get-date -f HH:mm:ss) All done!"